import React from 'react';
import ReactDOM from 'react-dom';

import './text-input.sass';
class TextInputField extends React.Component {

    state = {
        text: ''
    }

    onTextChange = (e) => {
        this.setState({
            text: e.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        if(this.state.text !== '')
            this.props.showData(this.state.text);
        else
            this.props.showData(null);
    };

    render() {
        return(
            <form
                className="form-group lyrics"
                onSubmit={this.onSubmit}>
                <input className="input-group input-group-lg" type="text" placeholder="Enter lyrics here!" onChange={this.onTextChange}/>
                <button className="btn btn-primary" type="submit">Submit</button>
            </form>
        );
    };
};

export default TextInputField;