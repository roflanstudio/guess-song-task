import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Main from '../main'
import Login from '../login'
import Register from '../register'
import Profile from '../profile';

import './app.sass';


class App extends React.Component {

    // state = {
    //     isSignedIn: false,
    //     token: ''
    // }

  
    render() {
        return (
          <Router>
                <Switch>
                    {/* <Route path='/' exact render={ (props) => {return <Main token={this.state.token} isSignedIn={this.state.isSignedIn} />}} /> */}
                    <Route path='/' exact component={Main} />
                    <Route path='/login' component={Login} />
                    <Route path='/logout' render={ () => {
                        localStorage.clear()
                        return <Redirect to='/'/> 
                        }} />
                    <Route path='/register' component={Register} />
                    <Route path='/profile/:id' component={Profile} />
                    {/* <Route component={NotFound} /> */}
                </Switch>
           </Router>
        );
      };
};

export default App;