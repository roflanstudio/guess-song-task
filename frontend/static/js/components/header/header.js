import React from 'react';

import './header.sass'
import { Link } from 'react-router-dom';

export default class Header extends React.Component {

    render_login = () => {
        if(this.props.isYou){
            return(
                <Link to='/logout'>
                    <i className="fas fa-sign-out-alt"></i>
                    <span> Logout </span>
                </Link>
            );
        }
        else if(!this.props.isSignedIn){
            return(
                <Link to='/login'>
                    <i className="fas fa-user"></i>
                    <span> Sign in </span>
                </Link>
            );
        } else {
            return(
                <Link to={`/profile/${localStorage.getItem('id')}`} >
                    <i className="fas fa-user-astronaut"></i>
                    <span> Hello, {localStorage.getItem('name')} </span>
                </Link> 
            );
        };
    };

    render_buttons = () => {
        //console.log(this.props)
        if(this.props.buttons){
            let status = this.props.status;
            if (this.props.isSignedIn){
                status = false;
            }
            if (status === null) {
                return (
                    <div className="buttons">
                    </div>
                )
            }
            else if (status === true || status === false){
                return (
                    <div className="buttons">
                    <button className="buttonHead" onClick={() => {this.props.byVoice()}}><span className="textHead">by sound</span></button>
                    <button className="buttonHead" onClick={() => {this.props.byText()}}><span className="textHead">by lyrics</span></button>
                </div>
                )
            }
        }
    }

    render() {
            return (
                <header>    
                    <div className='container'>
                        <Link to='/' className="title">
                            <i className="fas fa-music"></i>
                            <p>Le guess song</p>
                        </Link>
                        {this.render_buttons()}
                        <div className="login">
                            {this.render_login()}
                        </div>
                    </div>
                </header>
            );
    };
};