import React from 'react';
import { Link } from 'react-router-dom';

import FormErrors from '../form-errors/form-errors';
import './login.sass';
class Login extends React.Component{
    state = {
        email: '',
        password: '',
        errors: '',
        formErrors: {email: '', password: ''},
        emailValid: false,
        passwordValid: false,
        formValid: false
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
                      () => { this.validateField(name, value) });
    }


    render_errors = () => {
        if(this.state.errors !== '')
            return <p className='errorlist'> {this.state.errors} </p>
    }

    parse_errors = (object) =>{
        let result = '';
        for (const key in object) {
            for (const i of object[key]) {
                result += i + ' '
            }
        }
        return result;
    }

  	onSubmit = () => {
	  	fetch('/api/auth/', {
	      method: 'post',
	      headers: {'Content-Type': 'application/json'},
	      body: JSON.stringify({
	        "username": this.state.email,
	        "password": this.state.password
	      })
	    })
	      .then(response => response.json())
	      .then(user => {
            //console.log(user)
	        if (user.token) {
	        	localStorage.setItem('isSignedIn', 'true');
	        	localStorage.setItem('token', user.token);
	        	localStorage.setItem('id', user.user.id);
	        	localStorage.setItem('name', user.user.name);
	        	window.location.href = '/'
            }
            else this.setState({
                errors: this.parse_errors(user)
            })
	    })
    }
    
    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
    
        switch(fieldName) {
          case 'email':
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            fieldValidationErrors.email = emailValid ? '' : ' is invalid';
            break;
          case 'password':
            passwordValid = value.length >= 6;
            fieldValidationErrors.password = passwordValid ? '': ' is too short';
            break;
          default:
            break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        passwordValid: passwordValid
                      }, this.validateForm);
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid});
    }

    errorClass(error) {
        return(error.length === 0 ? '' : 'has-error');
    }

    render(){
        return (
            <div className='my-container'>
                <div className='form'>
                    <h1>Login</h1>
                    <div className="panel panel-default">
                        <FormErrors formErrors={this.state.formErrors} />
                    </div>
                    {this.render_errors()}
                    <div className="group"> 
                        <input 
                            type="text" 
                            className="login_email"
                            onChange={this.handleUserInput}
                            value={this.state.email}
                            name="email"
                            required/>
                            <span className="bar"></span>
                            <label>Email</label>
                    </div>
                    <div className="group">
                        <input 
                            type="password" 
                            className="login_password" 
                            onChange={this.handleUserInput}
                            value={this.state.password}
                            name="password"
                            required/>
                            <span className="bar"></span>
                            <label>Password</label>
                    </div>
                    <div className="input-form">
                        <button 
                            type="submit" 
                            className="btn btn-primary login_button"
                            disabled={!this.state.formValid}
                            onClick={this.onSubmit}>
                            Sign in     
                        </button>
                    </div>
                    <div className="reg-btn">
                        Don't have an account?
                        <Link to="/register">
                            Register
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;