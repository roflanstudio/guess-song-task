import React from 'react';

import './error-indicator.sass';


const ErrorIndicator = () => {
  return (
    <div className="error-indicator">
        <span className="boom">
          <i className="fas fa-bomb"></i>
            Oooops! 
          <i className="fas fa-bomb"></i>
        </span>
        <span>
            Track wasn't found or track part was too short!
        </span>
    </div>
  );
};

export default ErrorIndicator;