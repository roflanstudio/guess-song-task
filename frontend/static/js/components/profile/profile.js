import React from 'react'
import Header from '../header';

import album_art_anon from '../../../img/anon_music.png';
import './profile.sass'

class Profile extends React.Component{

    state = {
        isYou: localStorage.getItem('id') == this.props.match.params.id ? true : false,
        data: []
    }
    
    getData = () => {
        const {id} = this.props.match.params;
        fetch(`/api/user/${id}/records/`)
            .then(response => response.json())
            .then(obj => {
                this.setState({
                    data: obj
                })
            })
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.match.params.id !== prevProps.match.params.id) { 
           this.setState({
            isYou: localStorage.getItem('id') == this.props.match.params.id ? true : false
            });
            this.getData(); 
        }
      }

    componentDidMount() {
       this.getData();
    }  

    renderCards = (data) => {
        let result = [];
        let stop = 3;
        for (let index = 0; index < data.length; index+=3) {
            const slice = data.slice(index,stop);
            //console.log(slice);
            let temp = [];
            for (const obj of slice) {
                let album_art, album;
                if(obj.album_art != ''){
                    album_art = <img className="card-img-top" src={obj.album_art} alt="Card image cap"/>;
                }
                else{
                    album_art = <img className="card-img-top" src={album_art_anon} alt="Card image cap"/>;
                }
                if(obj.album != ''){
                    album = <p className="card-text">Album: {obj.album}</p>;
                }
                temp.push(
                    <div className="card">
                        {album_art}
                        <div className="card-body">
                        <h5 className="card-title">{obj.title}</h5>
                        <p className="card-text">Artist: {obj.artist}</p>
                        {album}
                        <p className="card-text"><a href={obj.youtube} className="">Watch on YouTube <i className="fab fa-youtube"></i> </a></p>
                        </div>
                    </div>
                )
            }
            while(temp.length < 3){
                temp.push(
                    <div className="card">
                    </div>
                )
            }
            result.push(
                <div className="card-group">
                    {temp}
                </div>
            )
            stop += 3;   
        }
        return result
    }
    
    render(){
        return (
            <div>
                <Header isSignedIn={localStorage.getItem('isSignedIn')} isYou={this.state.isYou}/>
                <div className='container'>
                    {this.renderCards(this.state.data)}
                </div>
            </div>
        )
    }
}

export default Profile;