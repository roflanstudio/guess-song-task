import React, { Fragment } from 'react';

import { ProgressBar } from 'react-fetch-progressbar';

import MicButton from '../mic-button';
import TextInputField from '../text-input';
import Player from '../player';
import Header from '../header';
import ErrorIndicator from '../error-indicator';

import './main.sass';

class Main extends React.Component {

    state = {
        res: null,
        url: null,
        text: !localStorage.getItem("isSignedIn")? null : false,
        isSignedIn: localStorage.getItem("isSignedIn"),
        token: localStorage.getItem("token"),
        error: false
    }

    error = () => {
        this.setState({
            error: true
        });
    };
    
    errorFalse = () => {
        this.setState({
            error: false
        });
    };

    guess = async (file) => {
        let data = new FormData();

        data.append('file', file)
        data.append('return','deezer,apple_music,spotify');
        data.append('api_token','e86ce77832f53bf60542afab198fefae');

        let res = await fetch('https://api.audd.io/', {
            method: 'POST',
            body: data
        })
            .then(res => res.ok ? res : Promise.reject(res))

        let obj = await res.text();
        obj = JSON.parse(obj);
        ////console.log(obj)
        // //console.log(obj.result.apple_music);
        if (obj.status != 'error')
        {
            if (obj.result !== null) {
                let url = "";
                let img = "";
                
                if (obj.result.spotify) {url = obj.result.spotify.external_urls.spotify; img = obj.result.spotify.album.images[0].url;}
                else if (obj.result.apple_music) {url = obj.result.apple_music.url; img = obj.result.apple_music.artwork.url;}
                else if (obj.result.deezer) {url = obj.result.deezer.link; img = obj.result.deezer.album.cover;}
                
                return ({
                    url: url, //obj.result.apple_music.previews[0].url,
                    img: img,
                    title: obj.result.title,
                    album: obj.result.album,
                    artist: obj.result.artist,
                    lyrics: ''
                })
            } else {
                return null;
            }
        }
        else {
            return null
        }  
    }

    onStop = async (recordedBlob) => {
        ////console.log('recordedBlob is: ', recordedBlob);
        let result = await this.guess(recordedBlob.blob);
        //console.log('onStop',result);
        if(result === null){
            this.error();
            return
        }
        this.setState({
            res: result
        });
        ////console.log(this.state);
        let link = await this.getYouTube(this.state.res.url);
        this.setState({
            url: link
        });
        this.errorFalse();
    };

    manageText = (string) => {
        this.errorFalse();

        if(string === null){
            this.error();
            return
        }

        fetch(`https://api.audd.io/findLyrics/?q=${string}&api_token=e86ce77832f53bf60542afab198fefae`)
            .then(res => res.ok ? res : Promise.reject(res))
            .then(response => response.json())
            .then(obj => {
              //console.log('Manage text',obj)
              if(obj.result.length>0){
                obj = obj.result[0];
                let media = JSON.parse(obj.media);
                //console.log('Manage text',obj,media);
                let url = media.length > 0 ? media.find(item => item.provider == 'youtube') : null;
                if(url === null){
                    this.error();
                    return
                }
                //console.log('Manage text',url);
                const result = {
                    url: '',
                    img: '',
                    title: obj.title,
                    album: '',
                    artist: obj.artist,
                    lyrics: obj.lyrics
                }
                if (url === undefined){
                    let sub_url = media.find(item => item.provider == 'spotify' || item.provider == 'soundcloud' );
                    if (sub_url == undefined) {
                        this.error();
                        return
                    }
                    fetch(`https://cors-anywhere.herokuapp.com/https://api.song.link/v1-alpha.1/links?url=${sub_url.url}&userCountry=UA`)
                    .then(res => res.ok ? res : Promise.reject(res))
                    .then(response_2 => response_2.json())
                    .then(sub_obj=>{
                        url = sub_obj.linksByPlatform.youtube.url;
                        this.setState({
                            res: result,
                            url: url
                        })
                    })
                }
                else {
                    this.setState({
                        res: result,
                        url: url.url
                    })
                }   
            }
            else{
                this.error();
                return
            }
          })
    }

    getYouTube = async (link) =>{
        let response = await fetch(`https://cors-anywhere.herokuapp.com/https://api.song.link/v1-alpha.1/links?url=${link}&userCountry=UA`)
            .then(res => res.ok ? res : Promise.reject(res))
        let result = await response.text();
        result = JSON.parse(result);
        //console.log('Get youtube',result);
        return result.linksByPlatform.youtube.url;
    };

    render_player = () => {
        if (this.state.url !== null){
            return <Player data={this.state.res} url={this.state.url}/>
        }
    }

    byVoice = () => {
        this.setState({
            text: false
        });
    }

    byText = () => {
        this.setState({
            text: true
        });
    }

    contentRender = () => {

        const errorMessage = this.state.error ? <ErrorIndicator /> : this.render_player();
        
        if (this.state.text === null) {
            return (
                <div className="container">
                    <div className="selection">
                        <span className="guess-title">Guess a song:</span>
                        <div className="buttons">
                            <button onClick={this.byVoice}><span className="text">by sound</span></button>
                            <span>or</span>
                            <button onClick={this.byText}><span className="text">by lyrics</span></button>
                        </div>
                    </div>
                </div>
            )
        } else if (this.state.text === false) {
            return (
                <Fragment>
                    <ProgressBar/>
                    <div className="container">
                        {errorMessage}  
                        <MicButton onStop={this.onStop}/>
                    </div>
                </Fragment>
            )
        } else if (this.state.text === true) {
            return (
                <Fragment>
                    <ProgressBar/>
                    <div className="container">
                        {errorMessage}   
                        <TextInputField showData={this.manageText}/>
                    </div>
                </Fragment>
            )
        };
    };

    render() {

        return(
            <div>
                <Header 
                    isSignedIn={this.state.isSignedIn} 
                    status={this.state.text} 
                    byVoice={this.byVoice} 
                    byText={this.byText} 
                    buttons={true}/>
                {this.contentRender()}
            </div>
        );
    };
};

export default Main;