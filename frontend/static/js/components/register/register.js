import React from 'react';
import { Link } from 'react-router-dom';

import './register.sass';
import FormErrors from '../form-errors/form-errors';

class Register extends React.Component{
    state = {
        email: '',
        first_name: '',
        last_name: '',
        password: '',
        repeatPassword: '',
        formErrors: {email: '', password: '', repeatPassword: ''},
        emailValid: false,
        passwordValid: false,
        repeatPasswordValid: false,
        formValid: false
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
                      () => { this.validateField(name, value) });
    }

    onFirstNameChange = (event) => {
    	this.setState({first_name: event.target.value});
      }
      
    onLastNameChange = (event) => {
    	this.setState({last_name: event.target.value});
  	}
    

  	onSubmit = () => {
        fetch('/api/auth/register/', {
            method: 'post',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
              "email": this.state.email,
              "first_name": this.state.first_name,
              "last_name": this.state.last_name,
              "password": this.state.password,
              "password2": this.state.repeatPassword
            })
          })
          .then(response => response.json())
	      .then(user => {
            //console.log(user)
	        if (user.token) {
	        	localStorage.setItem('isSignedIn', 'true');
	        	localStorage.setItem('token', user.token);
	        	localStorage.setItem('id', user.id);
	        	localStorage.setItem('name', user.name);
	        	window.location.href = '/'
            } else {
                //console.log(user);
            }
	    })
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let repeatPasswordValid = this.state.repeatPasswordValid;
    
        switch(fieldName) {
          case 'email':
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
            fieldValidationErrors.email = emailValid ? '' : ' is invalid';
            break;
          case 'password':
            passwordValid = value.length >= 6;
            fieldValidationErrors.password = passwordValid ? '': ' is too short';
            break;
          case 'repeatPassword':
            repeatPasswordValid = value === this.state.password;
            fieldValidationErrors.repeatPassword = repeatPasswordValid ? '': ' are not the same';
          default:
            break;
        }
        this.setState({formErrors: fieldValidationErrors,
                        emailValid: emailValid,
                        passwordValid: passwordValid,
                        repeatPasswordValid: repeatPasswordValid
                      }, this.validateForm);
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid});
    }

    errorClass(error) {
        return(error.length === 0 ? '' : 'has-error');
    }


    render(){
        return (
            <div className='my-container'>
                <div className='form'>
                    <h1>Register</h1>
                    <div className="panel panel-default">
                        <FormErrors formErrors={this.state.formErrors} />
                    </div>
                    <div className="group"> 
                        <input 
                            type="text" 
                            className="login_email"
                            onChange={this.handleUserInput}
                            value={this.state.email}
                            name="email"
                            required/>
                            <span className="bar"></span>
                            <label>Email</label>
                    </div>
                    <div className="group"> 
                        <input 
                            type="text" 
                            className="login_email"
                            onChange={this.onFirstNameChange}
                            required/>
                            <span className="bar"></span>
                            <label>First name</label>
                    </div>
                    <div className="group"> 
                        <input 
                            type="text" 
                            className="login_email"
                            onChange={this.onLastNameChange}
                            required/>
                            <span className="bar"></span>
                            <label>Last name</label>
                    </div>
                    <div className="group">
                        <input 
                            type="password" 
                            className="login_password" 
                            onChange={this.handleUserInput}
                            value={this.state.password}
                            name="password"
                            required/>
                            <span className="bar"></span>
                            <label>Password</label>
                    </div>
                    <div className="group">
                        <input 
                            type="password" 
                            className="login_password" 
                            onChange={this.handleUserInput}
                            value={this.state.repeatPassword}
                            name="repeatPassword"
                            required/>
                            <span className="bar"></span>
                            <label>Repeat password</label>
                    </div>
                    <div className="input-form">
                        <button 
                            type="submit" 
                            className="btn btn-primary login_button"
                            disabled={!this.state.formValid}
                            onClick={this.onSubmit}>
                            Sign in     
                        </button>
                    </div>
                    <div className="reg-btn">
                        Do you have an account already?
                        <Link to="/login">
                            Login
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default Register;