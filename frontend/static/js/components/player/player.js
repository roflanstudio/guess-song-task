import React, { Component } from 'react';
import ReactAudioPlayer from 'react-audio-player';
import ReactPlayer from 'react-player';

import './player.sass';

export default class Player extends Component {

    save = () => {
        if(localStorage.getItem('isSignedIn')){
            const props = this.props;
            fetch(`/api/user/${localStorage.getItem('id')}/records/`,{
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    "title": props.data.title,
                    "artist": props.data.artist,
                    "album": props.data.album,
                    "album_art": props.data.img,
                    "youtube": props.url,
                  })
            }
            )
        }
    }

    componentDidMount = () => {
        this.save();
    }
    
    componentDidUpdate = (prevProps, prevState) => {
        if(this.props.data != prevProps.data){
            this.save();
        }
    }

    render_album = () => {
        if(this.props.data.album != '')
            return <div>Album: {this.props.data.album}</div>
    }

    render_additional = () => {
        if(this.props.data.img != '')
            return <img src={this.props.data.img} alt="album art"/>
        // else if (this.props.data.lyrics != '')
        //     return <article> {this.props.data.lyrics} </article>
    }

    render () {
        return (
            <div className="player">
                <div className="trackData">
                    <div className="names">
                        <div className='title'>{this.props.data.title}</div>
                        <div>Artist: {this.props.data.artist}</div>
                        {this.render_album()}
                    </div>
                    {this.render_additional()}
                </div>
                <ReactPlayer
                    className="youTube"
                    url={this.props.url}
                    controls
                    // playing
                />
            </div>
        );
    };
};