import React from 'react';
import ReactDOM from 'react-dom';

import { ReactMic } from 'react-mic';
import './mic-button.sass';

class MicButton extends React.Component {
    state = {
        record: false
    }


    startRecording = () => {
        this.setState({
            record: true
        });

        setTimeout(this.stopRecording, 15000)
    }
    
    stopRecording = () => {
        this.setState({
            record: false
        });
    }
    
    renderControls = () => {
        if(this.state.record === false){
            return <button onClick={this.startRecording} type="button"><i className="fas fa-microphone-alt"></i></button> 
        }
        else{
            return <button onClick={this.stopRecording} id="rec-stop" type="button"><i className="fas fa-microphone-alt-slash"></i></button>
        }
    }

    render() {
        return (
            <div className="recording">
                <ReactMic
                    record={this.state.record}
                    className="sound-wave"
                    visualSetting="frequencyBars"
                    onStop={this.props.onStop}
                    strokeColor="#000000"
                    backgroundColor="white" />
                {this.renderControls()}
            </div>
        );
    }
};

export default MicButton;