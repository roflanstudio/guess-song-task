import React from 'react';
import ReactDOM from 'react-dom';
import { progressBarFetch, setOriginalFetch } from 'react-fetch-progressbar';
 
setOriginalFetch(window.fetch);
window.fetch = progressBarFetch;

import App from './components/App';


ReactDOM.render(<App/>,
  document.getElementById('root'));
