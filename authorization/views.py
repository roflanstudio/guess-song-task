from rest_framework.generics import CreateAPIView
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import AllowAny
from .serializers import RegisterSerializer, UserShortSerializer
from .models import User

class AuthTokenAPIView(ObtainAuthToken):
    '''
    Get authorization token for user
    '''
    renderer_classes = (JSONRenderer,BrowsableAPIRenderer)
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key,'user':UserShortSerializer(user).data})

class RegisterApiView(CreateAPIView):
    '''
    Register new user
    '''
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer