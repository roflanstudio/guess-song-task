from django.urls import path, re_path

from .views import AuthTokenAPIView, RegisterApiView

urlpatterns = [
    re_path(r'^$', AuthTokenAPIView.as_view()),
    path('register/', RegisterApiView.as_view()),
]
