from rest_framework import serializers
from rest_framework.authtoken.models import Token
from django.contrib.auth.password_validation import validate_password
from django.core import exceptions

from .models import User

class UserShortSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    class Meta:
        model = User
        fields = [
            'id',
            'name'
        ]

    def get_name(self,object):
        return str(object)

class RegisterSerializer(serializers.ModelSerializer): 
    password = serializers.CharField(write_only=True,style={'input_type':'password'})
    password2 = serializers.CharField(write_only=True,style={'input_type':'password'})
    token = serializers.SerializerMethodField(read_only=True)
    name = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = User
        fields = [
            'id',
            'email',
            'first_name',
            'last_name',
            'name',
            'token',
            'password',
            'password2',
        ]
        read_only_fields=['id']

    def get_token(self,object):
        return Token.objects.get_or_create(user=object)[0].key

    def get_name(self,object):
        return str(object)

    def validate_email(self,value):
        queryset = User.objects.filter(email__iexact=value)
        if queryset.exists():
            raise serializers.ValidationError({'email':["User with this email already exists"]})
        else:
            return value

    def validate(self, data):
        errors = dict()
        pw = data.get("password")
        if not pw is None:
            pw2 = data.pop("password2")
            if pw != pw2:
                errors['password'] = ["Passwords must match"]
        if errors:
            raise serializers.ValidationError(errors)
        return data

    def password_validation(self,user_obj,password):
        errors = dict()
        try:
            validate_password(password,user_obj)
        except exceptions.ValidationError as err:
            errors['password'] = list(err.messages)
        if errors:
            raise serializers.ValidationError(errors)

    def create(self, validated_data):
        user_obj = User(
            email = validated_data.get('email'),
            first_name = validated_data.get('first_name'),
            last_name = validated_data.get('last_name')
        )
        if not validated_data.get('password') is None: 
            self.password_validation(user_obj,validated_data.get('password'))
        user_obj.set_password(validated_data.get('password'))
        user_obj.save()
        return user_obj