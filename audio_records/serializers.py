from rest_framework import serializers
from .models import AudioRecord

class AudioRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = AudioRecord
        fields = '__all__'
        read_only_fields = ['owner',]

    def validate(self,data):
        val_data = super().validate(data)
        val_data['owner'] = self.context['user']
        return val_data