from django.apps import AppConfig


class AudioRecordsConfig(AppConfig):
    name = 'audio_records'
