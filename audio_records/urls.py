from django.urls import path, re_path

from .views import AudioRecordListApiView

urlpatterns = [
    path('', AudioRecordListApiView.as_view()),
]
