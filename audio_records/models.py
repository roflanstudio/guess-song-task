from django.db import models
from authorization.models import User

class AudioRecord(models.Model):
    title = models.CharField(max_length=150)
    artist = models.CharField(max_length=80)
    album = models.CharField(max_length=80,null=True,blank=True)
    album_art = models.URLField(null=True,blank=True)
    youtube = models.URLField(null=True,blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='records')

    def __str__(self):
        return self.title + ' by ' + self.artist