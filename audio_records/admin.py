from django.contrib import admin
from .models import AudioRecord

class RecordAdmin(admin.ModelAdmin):
    list_display = ('title','artist','album')

admin.site.register(AudioRecord, RecordAdmin)