from django.shortcuts import get_object_or_404
from rest_framework.generics import ListCreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from .serializers import AudioRecord, AudioRecordSerializer
from authorization.models import User

class AudioRecordListApiView(ListCreateAPIView):

    serializer_class = AudioRecordSerializer
    # permission_classes = [AllowAny,]

    def get_queryset(self):
        user_obj = get_object_or_404(User,pk=self.kwargs.get('user_pk'))
        return AudioRecord.objects.filter(owner=user_obj)

    def get_serializer_context(self):
        return {'user': get_object_or_404(User,pk=self.kwargs.get('user_pk'))}
